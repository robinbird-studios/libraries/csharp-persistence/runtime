﻿#region Disclaimer
// <copyright file="Persistence.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion

namespace RobinBird.Persistence.Runtime
{
    using System;
    using System.IO;
    using System.Text;
    using JetBrains.Annotations;

    /// <summary>
    /// Wrapper class that abstracts different serialization methods. The <seealso cref="Provider" /> property holds the real
    /// serialization logic. Once the serialization provider is setup you can blindly use this class to serialize and
    /// deserialize data. It makes it easy to switch serializers and makes it easy to call to serialization because serializer
    /// specific methods don't have to be remembered.
    /// </summary>
    public static class Persistence
    {
        /// <summary>
        /// Global serialization provider. The provider holds all the logic to actually run the serializer.
        /// You can either reassign this property to change to provider or use on of the overloaded methods
        /// to pass a different provider directly.
        /// </summary>
        public static IPersistenceProvider Provider { get; set; }

        /// <summary>
        /// Serialize data to disk. This overload is using a custom provider instead of the global one.
        /// </summary>
        /// <param name="provider">The custom provider to serialize with</param>
        /// <param name="path">Destination of the serialized file. Will override existing file.</param>
        /// <param name="o">Data to serialize.</param>
        /// <param name="type">The type of the data to serialize.</param>
        /// <exception cref="ArgumentNullException"><seealso cref="provider" /> is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException"><seealso cref="path" /> is null or empty.</exception>
        public static void Serialize([NotNull] IPersistenceProvider provider, string path, object o, Type type)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider", "Provider cannot be null.");
            }

            string pathWithExtension = ChangePathExtensionWithProvider(provider, path);

            if (string.IsNullOrEmpty(pathWithExtension))
            {
                throw new ArgumentOutOfRangeException("path", "Path cannot be empty or null");
            }

            // Create directories
            string dir = Path.GetDirectoryName(pathWithExtension);
            if (string.IsNullOrEmpty(dir) == false)
            {
                Directory.CreateDirectory(dir);
            }

            using (var f = new FileStream(pathWithExtension, FileMode.Create))
            {
                provider.Serialize(f, o, type);
            }
        }

        /// <summary>
        /// Serialize data to disk. Uses the globally configured <seealso cref="Provider" /> to serialize.
        /// </summary>
        /// <param name="path">Destination of the serialized file. Will override existing file.</param>
        /// <param name="o">Data to serialize.</param>
        /// <param name="type">The type of the data to serialize.</param>
        /// <exception cref="ArgumentOutOfRangeException"><seealso cref="path" /> is null or empty.</exception>
        public static void Serialize(string path, object o, Type type)
        {
            Serialize(Provider, path, o, type);
        }

        /// <summary>
        /// Serialize data to disk. Uses the globally configured <seealso cref="Provider" /> to serialize.
        /// </summary>
        /// <typeparam name="T">The type of the data to serialize.</typeparam>
        /// <param name="path">Destination of the serialized file. Will override existing file.</param>
        /// <param name="o">Data to serialize.</param>
        /// <exception cref="ArgumentOutOfRangeException"><seealso cref="path" /> is null or empty.</exception>
        public static void Serialize<T>(string path, object o)
        {
            Serialize(path, o, typeof (T));
        }

        /// <summary>
        /// Serialize data to disk. This overload is using a custom provider instead of the global one.
        /// </summary>
        /// <typeparam name="T">The type of the data to serialize.</typeparam>
        /// <param name="provider">The custom provider to serialize with</param>
        /// <param name="path">Destination of the serialized file. Will override existing file.</param>
        /// <param name="o">Data to serialize.</param>
        /// <exception cref="ArgumentNullException"><seealso cref="provider" /> is null.</exception>
        /// <exception cref="ArgumentOutOfRangeException"><seealso cref="path" /> is null or empty.</exception>
        public static void Serialize<T>(IPersistenceProvider provider, string path, object o)
        {
            Serialize(provider, path, o, typeof (T));
        }

        /// <summary>
        /// Serialize data to string. This overload is using a custom provider instead of the global one.
        /// </summary>
        /// <param name="provider">The custom provider to serialize with</param>
        /// <param name="o">Data to serialize.</param>
        /// <param name="encoding">Endoding to use for converting the string.</param>
        /// <param name="type">The type of the data to serialize.</param>
        /// <exception cref="ArgumentNullException"><seealso cref="provider" /> is null.</exception>
        public static string Serialize(IPersistenceProvider provider, object o, Encoding encoding, Type type)
        {
            string data;
            using (var memoryStream = new MemoryStream())
            using (var streamReader = new StreamReader(memoryStream, encoding))
            {
                provider.Serialize(memoryStream, o, type);
                memoryStream.Position = 0;
                data = streamReader.ReadToEnd();
            }
            return data;
        }

        /// <summary>
        /// Serialize data to string. This overload is using a custom provider instead of the global one.
        /// </summary>
        /// <param name="o">Data to serialize.</param>
        /// <param name="encoding">Endoding to use for converting the string.</param>
        /// <param name="type">The type of the data to serialize.</param>
        /// <exception cref="ArgumentNullException"><seealso cref="Provider" /> is null.</exception>
        public static string Serialize(object o, Encoding encoding, Type type)
        {
            return Serialize(Provider, o, encoding, type);
        }

        /// <summary>
        /// Serialize data to string. This overload is using a custom provider instead of the global one.
        /// </summary>
        /// <typeparam name="T">The type of the data to serialize.</typeparam>
        /// <param name="provider">The custom provider to serialize with</param>
        /// <param name="o">Data to serialize.</param>
        /// <param name="encoding">Endoding to use for converting the string.</param>
        /// <exception cref="ArgumentNullException"><seealso cref="provider" /> is null.</exception>
        public static string Serialize<T>(IPersistenceProvider provider, object o, Encoding encoding)
        {
            return Serialize(provider, o, encoding, typeof (T));
        }

        /// <summary>
        /// Serialize data to string. Using the global provider.
        /// </summary>
        /// <typeparam name="T">The type of the data to serialize.</typeparam>
        /// <param name="o">Data to serialize.</param>
        /// <param name="encoding">Endoding to use for converting the string.</param>
        /// <exception cref="ArgumentNullException"><seealso cref="Provider" /> is null.</exception>
        public static string Serialize<T>(object o, Encoding encoding)
        {
            return Serialize(Provider, o, encoding, typeof (T));
        }


        /// <summary>
        /// Returns deserialized data from file given with <seealso cref="path" /> parameter.
        /// This overload is using a custom provider instead of the global one.
        /// </summary>
        /// <param name="provider">The custom provider to serialize with.</param>
        /// <param name="path">Path to the file to deserialize.</param>
        /// <param name="type">Type of the data to deserialize.</param>
        /// <exception cref="ArgumentNullException"><seealso cref="provider" /> is null.</exception>
        /// <exception cref="FileNotFoundException"><seealso cref="path" /> is null/empty or does not exist.</exception>
        [NotNull]
        public static object Deserialize([NotNull] IPersistenceProvider provider, [NotNull] string path, [NotNull] Type type)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider", "Provider cannot be null.");
            }

            string pathWithExtension = ChangePathExtensionWithProvider(provider, path);
            if (File.Exists(pathWithExtension) == false)
            {
                throw new FileNotFoundException("Could not find file to deserialize.", pathWithExtension);
            }

            object obj;
            using (var f = new FileStream(pathWithExtension, FileMode.Open))
            {
                obj = provider.Deserialize(f, type);
            }
            return obj;
        }

        /// <summary>
        /// Returns deserialized data from file given with <seealso cref="path" /> parameter.
        /// This method is using the globally configure <seealso cref="Provider" /> to deserialize.
        /// </summary>
        /// <param name="path">Path to the file to deserialize.</param>
        /// <param name="type">Type of the data to deserialize.</param>
        /// <exception cref="FileNotFoundException"><seealso cref="path" /> is null/empty or does not exist.</exception>
        public static object Deserialize(string path, Type type)
        {
            return Deserialize(Provider, path, type);
        }

        /// <summary>
        /// Returns deserialized data from file given with <seealso cref="path" /> parameter.
        /// This method is using the globally configure <seealso cref="Provider" /> to deserialize.
        /// </summary>
        /// <typeparam name="T">Type of the data to deserialize.</typeparam>
        /// <param name="path">Path to the file to deserialize.</param>
        /// <exception cref="FileNotFoundException"><seealso cref="path" /> is null/empty or does not exist.</exception>
        public static T Deserialize<T>(string path)
        {
            return (T) Deserialize(path, typeof (T));
        }

        /// <summary>
        /// Returns deserialized data from file given with <seealso cref="path" /> parameter.
        /// This overload is using a custom provider instead of the global one.
        /// </summary>
        /// <typeparam name="T">Type of the data to deserialize.</typeparam>
        /// <param name="provider">The custom provider to serialize with.</param>
        /// <param name="path">Path to the file to deserialize.</param>
        /// <exception cref="ArgumentNullException"><seealso cref="provider" /> is null.</exception>
        /// <exception cref="FileNotFoundException"><seealso cref="path" /> is null/empty or does not exist.</exception>
        public static T Deserialize<T>(IPersistenceProvider provider, string path)
        {
            return (T) Deserialize(provider, path, typeof (T));
        }

        /// <summary>
        /// Returns deserialized data from string given with <seealso cref="data" /> parameter.
        /// This overload is using a custom provider instead of the global one.
        /// </summary>
        /// <param name="provider">The custom provider to serialize with.</param>
        /// <param name="data">Data in string do deserialize.</param>
        /// <param name="type">Type of the data to deserialize.</param>
        /// <param name="encoding"></param>
        /// <exception cref="ArgumentNullException"><seealso cref="provider" /> is null.</exception>
        public static object Deserialize(IPersistenceProvider provider, string data, Encoding encoding, Type type)
        {
            if (provider == null)
            {
                throw new ArgumentNullException("provider", "Provider cannot be null.");
            }

            object obj;
            using (var memoryStream = new MemoryStream(encoding.GetBytes(data)))
            {
                obj = provider.Deserialize(memoryStream, type);
            }
            return obj;
        }

        /// <summary>
        /// Returns deserialized data from string given with <seealso cref="data" /> parameter.
        /// This overload is using a custom provider instead of the global one.
        /// </summary>
        /// <param name="encoding">Encoding used for the string. Normally UTF-8.</param>
        /// <param name="data">Data in string do deserialize.</param>
        /// <param name="type">Type of the data to deserialize.</param>
        /// <exception cref="ArgumentNullException"><seealso cref="Provider" /> is null.</exception>
        public static object Deserialize(string data, Encoding encoding, Type type)
        {
            return Deserialize(Provider, data, encoding, type);
        }

        /// <summary>
        /// Returns deserialized data from string given with <seealso cref="data" /> parameter.
        /// This overload is using a custom provider instead of the global one.
        /// </summary>
        /// <typeparam name="T">Type of the data to deserialize.</typeparam>
        /// <param name="provider">The custom provider to serialize with.</param>
        /// <param name="encoding">Encoding used for the string. Normally UTF-8.</param>
        /// <param name="data">Data in string do deserialize.</param>
        /// <exception cref="ArgumentNullException"><seealso cref="provider" /> is null.</exception>
        public static T Deserialize<T>(IPersistenceProvider provider, string data, Encoding encoding)
        {
            return (T) Deserialize(provider, data, encoding, typeof (T));
        }

        /// <summary>
        /// Returns deserialized data from string given with <seealso cref="data" /> parameter.
        /// This overload is using a custom provider instead of the global one.
        /// </summary>
        /// <typeparam name="T">Type of the data to deserialize.</typeparam>
        /// <param name="data">Data in string do deserialize.</param>
        /// <param name="encoding">Encoding used for the string. Normally UTF-8.</param>
        /// <exception cref="ArgumentNullException"><see cref="Provider" /> is null.</exception>
        public static T Deserialize<T>(string data, Encoding encoding)
        {
            return (T) Deserialize(Provider, data, encoding, typeof (T));
        }

        #region Helper Methods
        private static string ChangePathExtensionWithProvider(IPersistenceProvider provider, string path)
        {
            if (Path.HasExtension(path) == false)
            {
                return Path.ChangeExtension(path, provider.Extension);
            }
            return path;
        }
        #endregion
    }
}