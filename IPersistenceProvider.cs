﻿#region Disclaimer

// <copyright file="IPersistenceProvider.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

namespace RobinBird.Persistence.Runtime
{
    using System;
    using System.IO;
    using JetBrains.Annotations;

    /// <summary>
    /// Every serialization method that wants to facilitate the <seealso cref="Persistence" /> wrapper has to implement this
    /// interface.
    /// </summary>
    public interface IPersistenceProvider
    {
        /// <summary>
        /// The extension of the serialized data. Without '.' so e.g. "json"
        /// </summary>
        string Extension { get; }

        /// <summary>
        /// Serialize given object into a stream.
        /// </summary>
        /// <param name="s">The stream to write to.</param>
        /// <param name="o">Object that is serialized.</param>
        /// <param name="t">The type of the object <seealso cref="o" /> to be serialized.</param>
        void Serialize([NotNull] Stream s, [NotNull] object o, [NotNull] Type t);

        /// <summary>
        /// Returns deserialized data.
        /// </summary>
        /// <param name="s">The stream to read from. This should lead to the serialized data to deserialize.</param>
        /// <param name="t">The type of the data to deserialize.</param>
        /// <returns></returns>
        object Deserialize(Stream s, Type t);

        // TODO: Evaluate if this is needed. Seems like the Extension property already covers this.
        /// <summary>
        /// Returns given path with the correct extension for serialized data by this provider.
        /// </summary>
        string GetPathWithExtension(string path);
    }
}