Provides an easy interface for many serializers in .NET

At the moment implemented:

- JSON
- XML
- Protobuf