﻿#region Disclaimer
// <copyright file="PersistenceIgnoreAttribute.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion

namespace RobinBird.Persistence.Runtime.Attributes
{
    using System;

    public class PersistenceIgnoreAttribute : Attribute
    {
    }
}